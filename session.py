import socket
import queue
import threading

from session import *
from constants import *
from robot import *

class Session(threading.Thread):
    def __init__(self, ip, port, sock):
        threading.Thread.__init__(self)
        self.stop = threading.Event()
        self.ip = ip
        self.port = port
        self.sock = sock
        self.buffer = queue.Queue(0)
        self.robot = Robot()
        self.unfinished = ""
        self.partial = False

    def run(self):
        print("Accepted client")
        if not self.authenticate():
            return
        print("-> NAVIGATION:")
        self.navigate()

    def _stop(self):
        self.sock.close()
        self.stop.set()
        self._is_stopped = True

    def authenticate(self):
        print("-> AUTHENTICATION:")
        self.sock.send(SERVER_USER.encode())
        print("-> SENT: " + SERVER_USER)
        while self.buffer.empty():
            if not self.receive():
                return False
        print("-> Received username")
        self.sock.send(SERVER_PASSWORD.encode())
        print("-> SENT: " + SERVER_PASSWORD)
        if self.verify():
            return True
        return False

    def receive(self):
        if not self.buffer.empty():  # still something in buffer
            return True

        listen = Listen(self.sock)
        listen.start()
        listen.join(TIMEOUT)

        if not listen.is_stopped():
            if listen.get_message() == "":
                if self.unfinished.find(str(CLIENT_RECHARGING[-2])) != -1:
                    self.wait()
                    return True
                print("No message received")
                listen.join(TIMEOUT)
                if listen.get_message() != "":
                    return self.parse_message()
                elif self.partial:
                    return listen.error(SERVER_SYNTAX_ERROR)
                print("Listen error")
                listen.set_stop(True)
                return self.error()
            else:
                print("Only partial message received: |" + str(listen.get_message()) + "|")
                ended = False
                while not ended:
                    listen.join(TIMEOUT)
                    if listen.is_stopped():
                        print("thread ended")
                        self.partial = True
                        listen.set_stop(True)
                        return self.parse_message(listen.get_message())
        else:
            if listen.is_fail():  # if syntax fail
                return self.error()
            if listen.is_charging():  # if charging starts
                self.wait()
                print("Charging finished")
            self.partial = False
        return self.parse_message(listen.get_message())

    def navigate(self):
        if self.locate():
            return
        while True:
            if self.move():
                break

    def verify(self):
        print("Verifying password...")
        username = self.buffer.get()
        print("username: " + username)
        while self.buffer.empty():
            self.receive()
        password = 0
        for symbol in username:
            password += ord(symbol)
        print("Password: " + str(password))

        rec_password = self.buffer.get()
        print("Password got: " + str(rec_password))
        if len(rec_password) > 5:
            return self.error(SERVER_SYNTAX_ERROR)
        if str(password) != rec_password:
            if rec_password.isdigit():
                return self.error(SERVER_LOGIN_FAILED)
            else:
                return self.error(SERVER_SYNTAX_ERROR)
        else:
            return self.sock.send(SERVER_OK.encode())
        return True

    def error(self, message=None):
        if message is not None:
            print("SENT: " + str(message))
            self.sock.send(message.encode())
        self.sock.shutdown(socket.SHUT_WR)
        print("Connection closed with error")
        self._stop()
        exit()
        return False

    def locate(self):
        self.sock.send(SERVER_TURN_RIGHT.encode())
        while self.buffer.empty():
            self.receive()
        self.set_position()
        self.robot.set_lines()
        if self.finish():
            return True
        moved = False
        while not moved:
            self.sock.send(SERVER_MOVE.encode())
            while self.buffer.empty():
                self.receive()
            self.set_position()
            moved = self.robot.is_moved()
        self.robot.set_direction()
        if self.finish():
            return True
        return False

    def set_position(self):
        pos = self.buffer.get()
        if pos[:3] != "OK ":
            self.error(SERVER_SYNTAX_ERROR)
        new_position = pos[3:].split()
        if pos[-1] == " ":
            return self.error(SERVER_SYNTAX_ERROR)
        if new_position[0].find(" ") != -1 or new_position[1].find(" ") != -1:
            return self.error(SERVER_SYNTAX_ERROR)
        if not new_position[1].isdigit():
            if not (new_position[1][1:].isdigit() and new_position[1][0] == "-"):
                return self.error(SERVER_SYNTAX_ERROR)
        if not new_position[0].isdigit():
            if not (new_position[0][1:].isdigit() and new_position[0][0] == "-"):
                return self.error(SERVER_SYNTAX_ERROR)
        self.robot.set_position(int(new_position[0]), int(new_position[1]))
        if not self.robot.is_moved():
            return True

    def finish(self):
        if self.robot.is_finished():
            self.sock.send(SERVER_PICK_UP.encode())
            while self.buffer.empty():
                self.receive()
            self.sock.send(SERVER_OK.encode())
            self._stop()
            exit()
            return True
        return False

    def move(self):
        if self.robot.can_move():
            self.sock.send(SERVER_MOVE.encode())
            while self.buffer.empty():
                self.receive()
            self.set_position()
            return self.finish()
        else:
            turn = self.robot.turn_to()
            print("Turn to: " + str(turn))
            while turn != 0:
                if turn > 0:
                    self.sock.send(SERVER_TURN_RIGHT.encode())
                    turn -= 1
                    self.robot.update_direction(1)
                else:
                    self.sock.send(SERVER_TURN_LEFT.encode())
                    turn += 1
                    self.robot.update_direction(-1)
                while self.buffer.empty():
                    self.receive()
                self.set_position()
            return self.finish()

    def wait(self):
        timeout = Timeout(self.sock)
        timeout.start()
        timeout.join(TIMEOUT_RECHARGING)

        if not timeout.is_stopped():
            print("Charging timeout ran out")
            while True:
                timeout.join(TIMEOUT_RECHARGING)
                if timeout.is_stopped():
                    message = timeout.get_buffer()
                    timeout.set_stop()
                    break
            message = message.replace(CLIENT_FULL_POWER, '')
            if message != '':
                self.parse_message(message)
            return True
        else:
            if timeout.is_error():
                return self.error(SERVER_LOGIC_ERROR)
            message = timeout.get_buffer()
            print("Buffer from timeout: " + message)
            if message == '':
                self.error()
            else:
                while True:
                    timeout.join(TIMEOUT_RECHARGING)
                    if timeout.is_stopped():
                        break
            message = message.replace(CLIENT_FULL_POWER, '')
            if message != '':
                self.parse_message(message)
            return True

    def parse_message(self, received):
        print("Parsing messages")
        if received == '':
            print("No new message received")
            return
        message = received
        messages = message.split("\r\n")

        self.unfinished += messages[0]
        print("Unfinished message: " + self.unfinished)

        if len(self.unfinished) > 98:
            print("Longer than allowed ...")
            return self.error(SERVER_SYNTAX_ERROR)

        if message.find("\r\n") != -1:
            print("Unfinished message finished: " + self.unfinished)
            if str(self.unfinished) == "FULL POWER":
                print("Got full power, continue")
                return True
            if str(self.unfinished) == "RECHARGING":
                print("Got recharging, must charge")
                self.unfinished = ""
                return self.wait()
            if str(self.unfinished).find(str(CLIENT_FULL_POWER[:-2])) != -1:
                print("Charging finished")
                self.unfinished = ""
                return True
            if str(self.unfinished).find(str(CLIENT_RECHARGING[:-2])) != -1:
                print("Charging started")
                self.unfinished = ""
                return self.wait()
            else:
                self.buffer.put(self.unfinished)
            self.unfinished = ""

        for mess in messages[1:-1]:
            if len(mess) > 98:
                self.error(SERVER_SYNTAX_ERROR)
            print("Putting: " + mess)
            self.buffer.put(mess)

        if message[-2:] == "\r\n" and len(messages) > 1 and messages[-1] != "":
            print("Putting: " + messages[-1])
            self.buffer.put(messages[-1])
            self.unfinished = ""
        else:
            self.unfinished = messages[-1]
        return True
