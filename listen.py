import socket
import queue
import threading

from session import *
from constants import *
from robot import *


class Listen(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.sock = sock
        self.stop = threading.Event()
        self.buffer = ""
        self.charging = False
        self.fail = False
        self.stop_listening = False

    def run(self):
        print("-> LISTENING MODE: ")
        self.buffer = ""
        while not self.stop_listening:
            if self.buffer.find("\r\n") != -1:
                if self.buffer.find(CLIENT_RECHARGING) != -1:
                    self.buffer = self.buffer.replace(CLIENT_RECHARGING, '')
                    self.charging = True
                self._is_stopped = True
                print("-> STOPPED")
                return
            elif len(self.buffer) > 99:
                return self.error(SERVER_SYNTAX_ERROR)
            self.buffer += self.sock.recv(BUFFER_SIZE).decode()
            if self.buffer.count(' ') == 3 and self.buffer.find("OK") != -1 and self.buffer.count("\r\n") <= 1:
                self._is_stopped = True
                self.fail = True
                self.error(SERVER_SYNTAX_ERROR)
                return

    def get_message(self):
        print(str(self.buffer))
        return self.buffer

    def is_charging(self):
        return self.charging

    def is_fail(self):
        return self.fail

    def _stop(self):
        self.stop.set()

    def is_stopped(self):
        return self._is_stopped

    def error(self, message=None):
        if message is not None:
            self.sock.send(message.encode())
        self._is_stopped = True
        self._stop()
        self.fail = True
        print("Error listening")
        self.sock.shutdown(socket.SHUT_WR)
        return False

    def set_stop(self, stop_listening):
        self.stop_listening = stop_listening
        self._stop()


class Timeout(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.sock = sock
        self.error = False
        self.stop = threading.Event()
        self.buffer = ""
        self.to_stop = False

    def run(self):
        print("-> CHARGING MODE")
        while not self.to_stop:
            self.buffer += self.sock.recv(BUFFER_SIZE).decode()
            if self.buffer.find(CLIENT_FULL_POWER) != -1:
                print("Charging ended")
                self._is_stopped = True
                return
            elif self.buffer != '' and self.buffer.find(CLIENT_FULL_POWER[0]) == -1:
                self.error = True
                return self.set_stop()

    def _stop(self):
        self.stop.set()

    def is_error(self):
        return self.error

    def is_stopped(self):
        return self._is_stopped

    def get_buffer(self):
        return self.buffer

    def set_stop(self):
        self._stop()
        self._is_stopped = True
        self.to_stop = True