import socket
import queue
import threading

from session import *
from constants import *

class Robot(object):
    def __init__(self):
        self.x = 0
        self.y = 0
        self.old_x = 0
        self.old_y = 0
        self.line_x = 0
        self.line_y = 0
        self.direction = 0

    def set_position(self, x, y):
        self.old_x = self.x
        self.old_y = self.y
        self.x = x
        self.y = y

    def set_direction(self):
        if self.x > self.old_x:
            self.direction = 0
        elif self.x < self.old_x:
            self.direction = 2
        elif self.y > self.old_y:
            self.direction = 3
        else:
            self.direction = 1
        print("Direction: " + str(self.direction))

    def can_move(self):
        if self.direction == self.line_x and self.y != 0:
            return True
        if self.direction == self.line_y and self.x != 0:
            return True
        return False

    def turn_to(self):
        turn_x = int((self.line_y - self.direction))
        if abs(turn_x) == 3:
            turn_x /= 3
        turn_y = int((self.line_x - self.direction))
        if abs(turn_y) == 3:       
            turn_y /= 3

        print("turn_X: " + str(turn_x) + " turn_Y: " + str(turn_y))
        if self.x != 0 and self.y != 0:
            if turn_x > turn_y:
                print(str(turn_x) + " > " + str(turn_y))
                return int(turn_y)
            else:
                print(str(turn_x) + " <= " + str(turn_y))
                return int(turn_x)
        elif self.x == 0:
            return int(turn_y)
        else:
            return int(turn_x)

    def is_moved(self):
        if self.x == self.old_x and self.y == self.old_y:
            return False
        return True

    def set_lines(self):
        if self.x > 0:
            self.line_y = 2
        else:
            self.line_y = 0

        if self.y > 0:
            self.line_x = 1
        else:
            self.line_x = 3
        print("Line x, y: " + str(self.line_x) + " " + str(self.line_y))

    def is_finished(self):
        if self.x == 0 and self.y == 0:
            return True
        return False

    def update_direction(self, add):
        self.direction = (self.direction + add) % 4
        print("Updated direction: " + str(self.direction))
