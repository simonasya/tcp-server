import socket
import queue
import threading

from session import *
from constants import *

class Server(object):

    def __init__(self):
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # self.sock.bind((socket.gethostname(), PORT))
        self.threads = []

    def accept(self):
        print("Server is listening")
        self.sock.listen(MAX_REQUESTS)
        count = 0
        while count < MAX_REQUESTS:
            (client, (ip, port)) = self.sock.accept()
            thread = Session(ip, port, client)
            thread.start()
            self.threads.append(thread)
            count += 1
        for thread in self.threads:
            thread.join()
        self.sock.close()
        print("Server stopped")

server = Server()
server.accept()
